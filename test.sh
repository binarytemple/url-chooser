#!/bin/sh

TEST_HOST=${1:-96.126.108.59}

function handle() {
 eval $@
}

python <<END | while read i; do handle $i; done
import urllib2

tmpl = 'curl -v "http://${TEST_HOST}:8080/url/%s"'
url1 = urllib2.quote("http://www.test.com/foo/?starter=3333")
url2 = urllib2.quote("http://www.test.com/foo/?starter=3333&main=4444")
url3 = urllib2.quote("http://www.test.com/foo/?starter=3333&main=5555&desert=66666")

print tmpl % url1
print tmpl % url2
print tmpl % url3
END
