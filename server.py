#!/usr/bin/python

import json
import bottle
from bottle import route, run, request, response, abort
from bottle import static_file, template
from collections import defaultdict
import urlparse

__doc__ = """
This is a minimal server, the purpose of which is:

Anyone can post URL's to it, run the test script to see valid example requests.

It will then return json, indicating if the request was valid.

Here are some valid URL's
http://www.test.com/foo/?starter=3333"
http://www.test.com/foo/?starter=3333&main=4444"
http://www.test.com/foo/?starter=3333&main=5555&desert=6666"

"""

@route('/url')
@route('/url/<name:re:.*>')
def hello(name=''):
    ''' 
    /url/<host url to check
    '''
    def n():
        return None

    response.headers.append("Access-Control-Allow-Origin", "*")
    response.content_type = "application/json"
    je = json.JSONEncoder()
    ret = {'result':"Error"}
    if name != "":
        response.status = 200
        res = defaultdict(n, urlparse.parse_qs(name))
        ret['result'] = "Success"
        ret['payload'] = res
    else:
        response.status = 500
    return je.encode(ret)

run(host='0.0.0.0', port=8080)
